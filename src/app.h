/**
 * @file app.hpp
 * @brief Responder for gas sensor.
 * @author Tomas Jakubik
 * @date Aug 21, 2020
 */

#ifndef APP_H_
#define APP_H_

#include <cstdint>
#include <chrono>


#define FW_VER    "L01.00.0003" //Firmware version of LimeSDR mini responder Lmajor.minor.patch

/**
 * @brief Run until "exit" is input or until error;
 * @param sernum LMS serial number to use
 * @param rx_gain radio Rx gain
 * @param tx_gain radio Tx gain
 * @return true on success
 */
bool app_loop(const char* sernum, float rx_gain, float tx_gain);

/**
 * @brief Time from program start for UserFace.
 * @return time from program start [ms]
 */
uint32_t app_get_time_ms();

/**
 * @brief Debug print info.
 */
void app_debug_print(int number, char letter);
void app_debug_print(int number, char letter, float value);



#endif /* APP_H_ */
