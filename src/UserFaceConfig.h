/**
 * @file UserFaceConfig_template.h
 * @brief Configuration for UserFace.
 * @note Copy this file, rename to UserFaceConfig.h and change something.
 * @author Tomas Jakubik
 * @date Jun 30, 2021
 */

#ifndef USERFACECONFIG_H_
#define USERFACECONFIG_H_

#include <app.h>
#include <iostream>

#define USERFACE_FW_NAME     "lime_response" //Firmware name of multi-frequency responder
#define USERFACE_FW_VER      FW_VER //Firmware version

#define USERFACE_NEWLINE     "\n"   //Newline string for UserFace

#define USERFACE_OUT(text)   do{std::cout << text;}while(0)  //Print text out to user

//Get time in milliseconds
#define USERFACE_TIME_MS()   (app_get_time_ms())

#endif /* USERFACECONFIG_H_ */
