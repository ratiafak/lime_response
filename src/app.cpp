/**
 * @file app.cpp
 * @brief Responder for gas sensor.
 * @author Tomas Jakubik
 * @date Aug 21, 2020
 */

#include <app.h>
#include <iomanip>
#include <iostream>
#include <fcntl.h>
#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <string>
#include <numbers>
#include <array>
#include <complex>  //Complex needs to be included before liquidsdr
#include <cstring>
#include <pthread.h>

#include <liquid/liquid.h>

#include <LmsUser.h>
#include <GmskGen.h>
#include <GmskRec.h>
#include <WrapFFT.h>
#include <Timed.h>
#include <UserFaceEngine.h>
#include <Scope.h>


using namespace std;
using namespace std::chrono_literals;

/**
 * @brief Measure time from program start.
 */
class FromStart
{
    std::chrono::system_clock::time_point start;
public:
    FromStart()
    {
        start = std::chrono::system_clock::now();
    }

    ///@return Get duration from program start.
    auto get_duration()
    {
        return std::chrono::system_clock::now() - start;
    }

    ///@return time from program start in milliseconds
    unsigned int get_ms()
    {
        return std::chrono::duration_cast<std::chrono::milliseconds>(get_duration()).count();
    }
};

/**
 * @brief Global variables and inter-thread communication.
 */
struct App
{
    //Config
    static const constexpr int    CHANNELS    = 64;     ///< Number of channels
    static const constexpr double BAUD_RATE   = 38400;  ///< Baud rate of the GMSK communication [symbols/s]
    static const constexpr int    FRAME_SIZE  = 1020;   ///< Size of a frame [complex samples]
    static const constexpr double SAMPLE_RATE = (BAUD_RATE*2)*(CHANNELS); ///< Sample rate [Hz]
    static const constexpr double CENTER_FREQ = 863542400 + (BAUD_RATE*2)*(CHANNELS/2);  ///< Center frequency is channel 32 [Hz]
    static const constexpr bool   PRINT_STATS = false;  ///< Whether to print LimeSDR stats each second

    //Transceivers
    GmskRec* recs[CHANNELS];    ///< Receiver part for all channels
    GmskGen* gen;   ///< Transmitter part
    unsigned int pause_delay;   ///< Pause receiver when transmitting, this sets delay of Rx and Tx

    //Thread sync
    enum class State
    {
        Run = 0,      ///< Run
        EndRadioError,///< EndRadioError
        EndUser,      ///< EndUser
    };
    State state = State::Run;       ///< Mark when app needs to end
    mutex app_wake_mutex;           ///< Mutex to synchronize new_data_cv
    condition_variable app_wake_cv; ///< Conditional variable to hold app thread until new data

    //Console print
    FromStart from_start;   ///< Measure time from program start
    chrono::system_clock::time_point last_receive;  ///< Time when last packet was received
    mutex cout_mutex;   ///< Mutex to synchronize cout

    //Stats
    lms_stream_status_t rx_status, tx_status; ///< Stats to print
    Timed gtx_op;       ///< Duration of GMSK Tx operation times SAMPLE_RATE/FRAME_SIZE
    Timed grx_op;       ///< Duration of GMSK Rx operation times SAMPLE_RATE/FRAME_SIZE
    Timed radio_op;     ///< Duration of the radio operation times SAMPLE_RATE/FRAME_SIZE
    Timed rx_op;        ///< Duration of the receiver operation times SAMPLE_RATE/FRAME_SIZE
    Timed fft_op;       ///< Duration of the fft operation times SAMPLE_RATE/FRAME_SIZE
    bool new_stats = false; ///< Marks new stat values

    App()
    {
        for (int i = 0; i < CHANNELS; i++)
        {
            recs[i] = new GmskRec(0x8E666F38);
        }
        gen = new GmskGen(0x8E666F38);
        pause_delay = 0;
    }

    virtual ~App()
    {
        for (int i = 0; i < CHANNELS; i++)
        {
            delete recs[i];
        }
        delete gen;
    }

    //Debug fifo for information codes
    struct TimeNumLet
    {
        uint32_t time;
        float value;
        uint8_t num;
        char let;
    };
    Fifo<TimeNumLet, 1024> debug_fifo;

} app;  ///< Global variables

/**
 * @brief Simplification for one radio frame.
 */
using Frame = std::array<std::complex<float>, App::FRAME_SIZE>;

/**
 * @brief Time from program start for UserFace.
 * @return time from program start [ms]
 */
uint32_t app_get_time_ms()
{
    return app.from_start.get_ms();
}

/**
 * @brief Debug print info.
 */
void app_debug_print(int number, char letter)
{
    app.debug_fifo.put(App::TimeNumLet{static_cast<uint32_t>(app.from_start.get_ms()), NAN, static_cast<uint8_t>(number), letter});
}

/**
 * @brief Debug print info.
 */
void app_debug_print(int number, char letter, float value)
{
    app.debug_fifo.put(App::TimeNumLet{static_cast<uint32_t>(app.from_start.get_ms()), value, static_cast<uint8_t>(number), letter});
}

/**
 * @brief Exit command.
 */
class ExitCommand: public UserFaceCommand
{
    App &app;   //Reference to global variables

    const char* name()
    {
        return "exit";
    }

    const char* info()
    {
        return " - End this program";
    }

    ///@param arg ignored
    void command(const char *arg)
    {
        std::lock_guard<std::mutex> lk(app.app_wake_mutex);
        app.state = App::State::EndUser;
        app.app_wake_cv.notify_all();    //Wake app thread because app ends
    }

public:
    ExitCommand(App& app) : app(app) { }
};

/**
 * @brief Loop to handle user and console io.
 */
void console_io_loop()
{
    //Timing for timestamps
    //chrono::system_clock::time_point timestamp_printed = chrono::system_clock::now();
    app.last_receive = chrono::system_clock::now();

    //Mark when to print newline between debug info
    bool mark_line = false;

    //Line of console input
    const constexpr int LINE_SIZE = 128;
    char line[LINE_SIZE];

    //Set console input nonblocking
    fcntl(0, F_SETFL, fcntl(0, F_GETFL) | O_NONBLOCK);

    while (app.state == App::State::Run)
    {
        this_thread::sleep_for(chrono::milliseconds(100));

        //Input
        if (fgets(line, LINE_SIZE, stdin) != nullptr)
        {
            size_t len = strlen(line);
            if (len > 1)
            {
                if (line[len - 1] == '\n')
                {
                    len--;
                }
                lock_guard<mutex> lk(app.cout_mutex);
                UserFaceEngine::in(line, len);
            }
        }

        //Output
        if (app.PRINT_STATS && app.new_stats)
        {
            lock_guard<mutex> lk(app.cout_mutex);

            cout << endl;
            cout << dec;
            cout << setfill(' ');
            cout << "                     avg        max        min" << endl;
            cout << "Radio takes = "; app.radio_op.print(); cout << endl;
            cout << "Rx takes    = "; app.rx_op.print(); cout << endl;
            cout << "GRx takes   = "; app.grx_op.print(); cout << endl;
            cout << "GTx takes   = "; app.gtx_op.print(); cout << endl;
            cout << "FFT takes   = "; app.fft_op.print(); cout << endl;
            cout << "Sum         = " << app.radio_op.latched_avg + app.rx_op.latched_avg + app.fft_op.latched_avg << " us" << endl;
            cout << "frame_size  = " << app.FRAME_SIZE << " (" << 1000000.0*app.FRAME_SIZE/app.SAMPLE_RATE << " us)" << endl;
            cout << "-----------------         RX         TX" << endl;
            cout << "active          = " << std::setw(10) << app.rx_status.active          << " " << std::setw(10) << app.tx_status.active          << endl;
            cout << "droppedPackets  = " << std::setw(10) << app.rx_status.droppedPackets  << " " << std::setw(10) << app.tx_status.droppedPackets  << endl;
            cout << "fifoFilledCount = " << std::setw(10) << app.rx_status.fifoFilledCount << " " << std::setw(10) << app.tx_status.fifoFilledCount << endl;
            cout << "fifoSize        = " << std::setw(10) << app.rx_status.fifoSize        << " " << std::setw(10) << app.rx_status.fifoSize        << endl;
            cout << "linkRate        = " << std::setw(10) << app.rx_status.linkRate        << " " << std::setw(10) << app.tx_status.linkRate        << endl;
            cout << "overrun         = " << std::setw(10) << app.rx_status.overrun         << " " << std::setw(10) << app.tx_status.overrun         << endl;
            cout << "underrun        = " << std::setw(10) << app.rx_status.underrun        << " " << std::setw(10) << app.tx_status.underrun        << endl;

            app.new_stats = false;
        }

        //Debug info
        {
            lock_guard<mutex> lk(app.cout_mutex);
            App::TimeNumLet timenumlet;
            while (app.debug_fifo.pop(timenumlet))
            {
                if (timenumlet.let == '_')
                {
                    if (mark_line == false)
                    {
                        cout << endl;
                        mark_line = true;
                    }
                }
                else
                {
                    if (mark_line)
                    {
                        printf("@ %7u.%03u s - debug ", timenumlet.time / 1000, timenumlet.time % 1000);
                        mark_line = false;
                    }
                    cout << "'" << timenumlet.let << "'" << (int)timenumlet.num;
                    if(isnan(timenumlet.value) == false)
                    {
                        cout << "(" << timenumlet.value << ")";
                    }
                    cout << " ";
                }
            }
        }

        //Mark when no data are received
        /*//if (((chrono::system_clock::now() - app.last_receive) > 60s)
        //    && ((chrono::system_clock::now() - timestamp_printed) > 60s))
        if (((chrono::system_clock::now() - app.last_receive) > 8s)
            && ((chrono::system_clock::now() - timestamp_printed) > 8s))
        {
            lock_guard<mutex> lk(app.cout_mutex);
            unsigned int now = app.from_start.get_ms();
            timestamp_printed = chrono::system_clock::now();
            printf("@ %7u.%03u s - No data for %7li s\r\n",
                     now / 1000, now % 1000,
                     chrono::duration_cast<chrono::seconds>(chrono::system_clock::now() - app.last_receive).count());
        }*/
    }
}

/**
 * @brief Print binary data.
 * @param info string printed before the data
 * @param time timestamp printed before info
 * @param data data to print
 * @param len length of data
 */
static void app_print_hex(const char* info, uint32_t time, const uint8_t* data, uint32_t len)
{
    printf("@ %7u.%03u s - %s %u B: ", time / 1000, time % 1000, info, len);
    for(uint32_t i = 0; i < len; i++)
    {
        printf("0x%02x ", data[i]);
    }
    printf("\n");
}

/**
 * @brief Print data from packet from sensor.
 * @param packet packet to print
 * @param len length of packet
 */
static void app_print_data(const gather_t *packet, uint32_t len)
{
    uint16_t timestamp;
    int payload_idx = 0;

    //Print secondly items
    if(packet->secondly > 0)
    {
        timestamp = packet->payload[payload_idx++];
        for (;   //Start after timestamp
            (payload_idx < (packet->secondly + 1))  //Only this many secondly items
                && (len >= (sizeof(gather_t) + sizeof(uint16_t) * ((payload_idx + 1) - GATHER_PAYLOAD_SIZE))); //Only for packet length
            payload_idx++)
        {
            printf("\"Data\", %u, %u, %i.%1u, %u, %i\n",
                     packet->proto.id,
                     timestamp,
                     packet->temperature / 10,
                     (packet->temperature > 0) ? (packet->temperature % 10) : ((-1 * packet->temperature) % 10),
                     packet->humidity,
                     static_cast<int16_t>(packet->payload[payload_idx]));
            timestamp += 1;
        }
    }

    //Print minutely items
    timestamp = packet->payload[payload_idx++];
    for (;   //Start after timestamp
        (len >= (sizeof(gather_t) + sizeof(uint16_t) * ((payload_idx + 1) - GATHER_PAYLOAD_SIZE))); //Only for packet length
        payload_idx++)
    {
        printf("\"Data\", %u, %u, %i.%1u, %u, %i\n",
                 packet->proto.id,
                 timestamp,
                 packet->temperature / 10,
                 (packet->temperature > 0) ? (packet->temperature % 10) : ((-1 * packet->temperature) % 10),
                 packet->humidity,
                 static_cast<int16_t>(packet->payload[payload_idx]));
        timestamp += 60;
    }
}

/**
 * @brief Receive packet and respond.
 * @param ch received on this channel
 * @param rx_data received data
 * @param len number of bytes in data
 * @return true if Tx was started
 */
bool app_receive_packet(int ch, const uint8_t* rx_data, uint32_t len)
{
    //Map data to packet
    const proto_t* proto = reinterpret_cast<const proto_t*>(rx_data);
    const request_t* request = reinterpret_cast<const request_t*>(rx_data);
    const gather_t* gather = reinterpret_cast<const gather_t*>(rx_data);
    const extra_t* extra = reinterpret_cast<const extra_t*>(rx_data);

    bool txed = false;   //Return true if Tx started

    unsigned int now = app.from_start.get_ms();

    //Ignore packets sent by responder
    if ((proto->header == HEADER_ACK) || (proto->header == HEADER_CONTROL))
    {
        printf("@ %7u.%03u s - Error, own packet received\n", now / 1000, now % 1000);
        return false;
    }

    //Check valid header
    if ((proto->header != HEADER_REQUEST) && (proto->header != HEADER_GATHER) && (proto->header != HEADER_EXTRA))
    {
        app_print_hex("Error, bad header", now, rx_data, len);
        return false;
    }

    //Check length
    if (((proto->header == HEADER_REQUEST) && (len != sizeof(request_t)))
        || ((proto->header == HEADER_GATHER) && ((len < (sizeof(gather_t) - GATHER_PAYLOAD_SIZE * sizeof(uint16_t)))
            || (len > sizeof(gather_t))))
        || ((proto->header == HEADER_EXTRA) && (len != sizeof(extra_t))))
    {
        printf("@ %7u.%03u s - Error, packet with wrong length\n", now / 1000, now % 1000);
        return false;
    }

    //Check sensor id
    if ((proto->id == 0) || (proto->id > UserFaceEngine::Config::CONFIGS_N))
    {
        printf("@ %7u.%03u s - Error, unknown sensor id %u\n",
                 now / 1000, now % 1000,
                 proto->id);
        return false;
    }

    //Get correct channel where the packet was sent
    int32_t received_channel = 0;
    switch (proto->header)
    {
        case HEADER_REQUEST: received_channel = request->channel; break;
        case HEADER_GATHER:  received_channel = gather->channel;  break;
        case HEADER_EXTRA:   received_channel = extra->channel;   break;
    }
    if( received_channel >= app.CHANNELS)
    {
        printf("@ %7u.%03u s - Error, sensor %u sent on channel %u\n",
                 now / 1000, now % 1000,
                 proto->id, received_channel);
        return false;
    }
    if (received_channel != ch)
    {
        printf("@ %7u.%03u s - Warning, packet received on %u instead of %u\n",
                 now / 1000, now % 1000,
                 ch, received_channel);
    }

    //Get the right sensor
    UserFaceEngine::Config::SensorControl *sensor = &UserFaceEngine::user_config.sensor_controls[proto->id - 1];

    //Respond
    if ((proto->header == HEADER_REQUEST)  //Sensor requested control
        || sensor->changed)  //Config changed
    {
        sensor->config.proto.seq = proto->seq;  //Respond with the same seq
        if (app.gen->set_data(reinterpret_cast<uint8_t*>(&sensor->config), sizeof(control_t),
                              (received_channel - 32) * (2.0f * numbers::pi / 64.0f)))
        {
            txed = true;

            if (proto->header == HEADER_REQUEST)  //Sensor requested config
            {
                char fw_ver[17];
                strncpy(fw_ver, request->fw_ver, 16);   //Get safe FW_VER string
                fw_ver[16] = '\0';
                printf("@ %7u.%03u s - Sensor %u restarted on ch %u, fw=\"%s\"\n", now / 1000,
                         now % 1000, proto->id, received_channel, fw_ver);
            }
            else
            {
                printf("@ %7u.%03u s - Packet from %u on ch %u, config sent\n", now / 1000,
                         now % 1000, proto->id, received_channel);
            }
        }
        else
        {
            if (proto->header == HEADER_REQUEST)  //Sensor requested config
            {
                char fw_ver[17];
                strncpy(fw_ver, request->fw_ver, 16);   //Get safe FW_VER string
                fw_ver[16] = '\0';
                printf("@ %7u.%03u s - Sensor %u restarted on ch %u, fw=\"%s\"\n", now / 1000,
                         now % 1000, proto->id, received_channel, fw_ver);
            }
            printf("@ %7u.%03u s - Error, config not sent to %u on ch %u because of ongoing Tx\n",
                     now / 1000, now % 1000, proto->id, received_channel);
        }
    }
    else    //Regular ACK is to be sent
    {
        ack_t ack = { .proto = { .header = HEADER_ACK,
                                 .id = proto->id,
                                 .seq = proto->seq } };   //Respond with the same seq and id

        //Send
        if (app.gen->set_data(reinterpret_cast<uint8_t*>(&ack), sizeof(ack_t),
                              (received_channel - 32) * (2.0f * numbers::pi / 64.0f)))
        {
            txed = true;

            printf("@ %7u.%03u s - Packet from %u on ch %u, ack sent\n",
                     now / 1000, now % 1000, proto->id, received_channel);
        }
        else
        {
            printf("@ %7u.%03u s - Error, packet from %u on ch %u not acked because of ongoing Tx\n",
                     now / 1000, now % 1000, proto->id, received_channel);
        }
    }

    //Check seq
    if (proto->header == HEADER_REQUEST)
    {
        sensor->seq = proto->seq;   //Store seq
        app.last_receive = chrono::system_clock::now();   //Mark successful communication
    }
    else if (proto->seq == ((sensor->seq + 1) & 0x3))  //One more would be ok, other is bad sequence number
    {
        sensor->seq = proto->seq;   //Store seq
        app.last_receive = chrono::system_clock::now();   //Mark successful communication

        //Print changes in configuration
        if ((sensor->sent == true) && (sensor->changed == false))
        {
            sensor->sent = false;
            printf("@ %7u.%03u s - Config applied in %u\n",
                     now / 1000, now % 1000, proto->id);
        }

        //Debug trigger scopes for missing packets
        if (proto->header == HEADER_GATHER)
        {
            for(int i = gather->secondly; i > 0; i--)
            {
                if (static_cast<int16_t>(gather->payload[i]) != -1)
                {
                    printf("@ %7u.%03u s - Missed packet on %u\n",
                             now / 1000, now % 1000, gather->payload[i]);
                    break;
                }
            }
        }
    }
    else if (proto->seq == sensor->seq)   //Repeated packet
    {
        printf("@ %7u.%03u s - Repeated packet from %u\n",
                 now / 1000, now % 1000, proto->id);
    }
    else    //Completely wrong sequence
    {
        printf("@ %7u.%03u s - Error, bad sequence from %u, possibly restart\n",
                 now / 1000, now % 1000, proto->id);
    }

    //Print packet data
    if (proto->header == HEADER_GATHER)
    {
        app_print_data(gather, len);
    }
    else if (proto->header == HEADER_EXTRA)
    {
        printf("@ %7u.%03u s - Extra from %u, nearest timestamp %u, extra seq %u, value %i\n",
                 now / 1000, now % 1000,
                 proto->id,
                 extra->nearest_timestamp,
                 extra->extra_seq,
                 static_cast<int16_t>(extra->lmp));
    }

    //Mark sent config
    if ((proto->header == HEADER_REQUEST) || sensor->changed)
    {
        sensor->changed = false;
        sensor->sent = true;
    }

    return txed;
}

/**
 * @brief Loop to handle hardware, early stages of receiver and transmitter.
 * @param app reference to global variables
 * @param sernum LMS serial number to use
 * @param rx_gain radio Rx gain
 * @param tx_gain radio Tx gain
 */
void transceiver_loop(const char *sernum, float rx_gain, float tx_gain)
{
    //Create and init LimeSDR
    LmsUser lms(App::SAMPLE_RATE,    //sample_rate
                App::CENTER_FREQ,    //center_freq
                App::FRAME_SIZE,     //frame_size
                8*App::FRAME_SIZE,   //fifo_size
                rx_gain,            //rx_gain
                tx_gain,            //tx_gain
                8*App::FRAME_SIZE);  //rx_tx_delay
    if (lms.init(sernum) == false)
    {
        std::lock_guard<std::mutex> lk(app.app_wake_mutex);
        app.state = App::State::EndRadioError;
        app.app_wake_cv.notify_all();    //Wake app thread because radio ended
        return;
    }

    //Create input and output buffers
    int gmsk_in_offset = 0;     ///< How many samples remain from previous frame
    complex<float> frame_in[App::FRAME_SIZE];   ///< Input time data
    complex<float> frame_out[App::FRAME_SIZE];  ///< Output time data
    complex<float> time_in[App::CHANNELS];   ///< Input time data
    complex<float> spec_in[App::CHANNELS];   ///< Input FFT spectrum
    WrapFFT<float> gmsk_fft(App::CHANNELS, time_in, spec_in, false);

    //Stream
    if(lms.stream_start() == false)
    {
        std::lock_guard<std::mutex> lk(app.app_wake_mutex);
        app.state = App::State::EndRadioError;
        app.app_wake_cv.notify_all();    //Wake app thread because radio ended
    }
    while (app.state == App::State::Run)
    {
        //GMSK Tx
        app.gtx_op.start();
        for (int i = 0; i < App::FRAME_SIZE; i++)
        {
            //Run transmitter
            if (app.gen->get_sample(frame_out[i]))
            {
                app.pause_delay = 1 + 8;  //Set delay between Tx and preamble detection on Rx
            }

        }
        if (app.pause_delay > 0)
        {
            app.pause_delay--;
            if (app.pause_delay == 0)   //After a delay
            {
                for (auto rec : app.recs)
                {
                    rec->pause();   //Pause all receivers
                }
            }
        }
        app.gtx_op.stop();

        //Handle radio
        app.radio_op.start();
        if (lms.stream(frame_in, frame_out) == false)
        {
            std::lock_guard<std::mutex> lk(app.app_wake_mutex);
            app.state = App::State::EndRadioError;
            app.app_wake_cv.notify_all();    //Wake app thread because radio ended
            break;
        }
        app.radio_op.stop();

        //GMSK Rx
        app.rx_op.start();
        for (int frame_pos = 0; frame_pos < (App::FRAME_SIZE + App::CHANNELS); frame_pos += App::CHANNELS)
        {
            //Copy data from frame to time_in
            if (frame_pos < gmsk_in_offset)    //First partial set
            {
                memcpy(&time_in[gmsk_in_offset], frame_in, sizeof(complex<float>)*(App::CHANNELS - gmsk_in_offset));
            }
            else if ((frame_pos - gmsk_in_offset + App::CHANNELS) <= App::FRAME_SIZE)   //Full sets
            {
                memcpy(time_in, &frame_in[frame_pos - gmsk_in_offset], sizeof(complex<float>)*App::CHANNELS);
            }
            else    //Remainder
            {
                int new_offset = App::FRAME_SIZE - (frame_pos - gmsk_in_offset);
                memcpy(time_in, &frame_in[frame_pos - gmsk_in_offset], sizeof(complex<float>)*new_offset);
                gmsk_in_offset = new_offset;
                break;  //Will be processed with the next frame
            }

            //Do FFT for GMSK Rx
            app.fft_op.start();
            gmsk_fft.execute();
            app.fft_op.stop();

            //Handle early stages of Rx
            app.grx_op.start();
            for (int i = 0; i < App::CHANNELS; i++)
            {
                //Flip spectrum halves to have frequencies fitting channel numbers
                int flip_spec = i + App::CHANNELS/2;
                if (flip_spec >= App::CHANNELS)
                {
                    flip_spec -= App::CHANNELS;
                }

                //Run receiver
                app.recs[i]->put_sample(spec_in[flip_spec]);
            }
            app.grx_op.stop();
        }
        app.rx_op.stop();

        //Wakeup main thread
        {
            std::lock_guard<std::mutex> lk(app.app_wake_mutex);
            app.app_wake_cv.notify_all();    //Wake app thread because new data are available
        }

        //Store stats
        if (app.PRINT_STATS && (app.new_stats == false) && (app.fft_op.cnt >= (app.SAMPLE_RATE / app.FRAME_SIZE)))
        {
            lms.status(&app.rx_status, &app.tx_status);
            app.fft_op.latch();
            app.rx_op.latch();
            app.grx_op.latch();
            app.gtx_op.latch();
            app.radio_op.latch();

            app.new_stats = true;
        }

        //Debug mark frames
        app.debug_fifo.put(App::TimeNumLet{0, NAN, 0,'_'});
    }
}


/**
 * @brief Loop to handle later stages of receiver and packet handling.
 * @param sernum LMS serial number to use
 * @param rx_gain radio Rx gain
 * @param tx_gain radio Tx gain
 */
bool app_loop(const char* sernum, float rx_gain, float tx_gain)
{
    //Modify default sensor settings
    for (int i = 0; i < UserFaceEngine::Config::CONFIGS_N; i++)
    {
        UserFaceEngine::user_config.sensor_controls[i].config.multi_f = 1;  //Enable hopping for all
    }

    //Start thread for console input and output
    ExitCommand exit_command(app);  //Add command to exit the app
    thread console_io_thread(console_io_loop);

    //Set priority
    sched_param sch = {sched_get_priority_max(SCHED_FIFO)};
    if (pthread_setschedparam(pthread_self(), SCHED_FIFO, &sch))
    {
        cerr << "Failed to set thread priority: " << strerror(errno) << '\n';
        //Continue with regular priority for debugging
    }

    //Start thread to process transceiver, also with high priority
    thread transceiver_thread(transceiver_loop, sernum, rx_gain, tx_gain);

    while (app.state == App::State::Run)
    {
        //Continue processing receiver
        for (int i = 0; i < app.CHANNELS; i++)
        {
            if (app.recs[i]->process() == GmskRec::State::Done)  //Process the receiver and check for new packet
            {
                uint8_t data[128];
                const uint8_t* packet = app.recs[i]->get_data();  //Get received data
                size_t len = min<uint8_t>(packet[0], 128); //Not counting length byte itself and limited to 128 B
                memcpy(data, &packet[1], len);  //Copy outside

                app.recs[i]->resume();   //Reenable receiver

                lock_guard<mutex> lk(app.cout_mutex);
                app_receive_packet(i, data, len);  //Process packet and respond
            }
        }

        //Wait for new samples
        {
            unique_lock<mutex> lk(app.app_wake_mutex);
            app.app_wake_cv.wait(lk, []
            {
                if (app.state != App::State::Run)
                {
                    return true;
                }
                for (auto rec : app.recs)
                {
                    if (rec->something_to_process())
                    {
                        return true;
                    }
                }
                return false;
            });  //Wait for new samples
        }
    }

    //Join with the other threads
    transceiver_thread.join();
    console_io_thread.join();

    //End with success if user exited
    return app.state == App::State::EndUser;
}
