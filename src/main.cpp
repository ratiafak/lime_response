/**
 * @file main.cpp
 * @brief TODO
 * @author Tomas Jakubik
 * @date Aug 21 2020
 */

#include <app.h>
#include <iostream>

using namespace std;

/**
 * @brief Main.
 * @return nonzero on error
 */
int main(int argc, char *argv[])
{
    //My serial numbers
    //Piece 1 - 1D538AB9CFF24C
    //Piece 2 - 1D5387FB9822DC

    const char* sernum = nullptr;
    if (argc > 1)
    {
        sernum = argv[1];
    }
    float rx_gain = 70;
    if (argc > 2)
    {
        rx_gain = atof(argv[2]);
    }
    float tx_gain = 55;
    if (argc > 3)
    {
        tx_gain = atof(argv[3]);
    }

    cout << "Running " << FW_VER << endl;
    cout << "Using Rx gain = " << rx_gain << " dB and Tx gain = " << tx_gain << " dB" << endl;

    if (app_loop(sernum, rx_gain, tx_gain))
    {
        cout << "Success" << endl;
        return 0;
    }

    return -1;
}


