/**
 * @file Scope.h
 * @brief Store signal in buffer, trigger and print.
 * @author Tomas Jakubik
 * @date Apr 29, 2021
 */

#ifndef SCOPE_H_
#define SCOPE_H_

#include <iostream>
#include <fstream>
#include <array>

/**
 * @brief Template to gather samples and print them as a matlab array.
 * @tparam T data type of signal
 */
template<class T>
class Scope
{
    int in;             ///< Current in position
    int trig;           ///< Offset of the trigger
    bool triggered;     ///< True when triggered and sampling the rest of window
    std::string trig_comment;   ///< Comment the triggered scope
    bool wait_print;    ///< True when gathered enough and waiting to print
    int trig_print;     ///< Position where to print when triggered
    std::ofstream& logfile;  ///< Where log is written
    std::string name;   ///< Printed variable name
    const size_t len;   ///< Size of the scope buffer
    T* buffer;          ///< Buffer for sampled data

public:
    /**
     * @brief Init scope.
     * @param logfile where to write the scope
     * @param name printed variable name
     * @param size of scope in samples
     * @param trig offset of the trigger
     */
    Scope(std::ofstream& logfile, std::string name, size_t len, int trig = 0) :
        in(0), trig(trig), triggered(false), wait_print(false), trig_print(0), logfile(logfile), name(name), len(len)
    {
        buffer = new T[len];
    }

    virtual ~Scope()    ///< Nothing to destruct
    {
        delete[] buffer;
    }

    /**
     * @brief Function to print one sample to log.
     * @param logfile write to this stream
     * @param sample write this sample
     */
    virtual void print_one_sample(std::ofstream& logfile, const T& sample) = 0;

    /**
     * @brief Change trigger offset.
     * @param new_trig offset of the trigger
     */
    void setTrig(unsigned int new_trig)
    {
        trig = new_trig;
    }

    /**
     * @brief Reset if triggered.
     */
    void reset()
    {
        triggered = false;
    }

    /**
     * @brief Trigger and print data.
     * @param comment comment the scope
     */
    void trigger(std::string comment)
    {
        if (triggered == false)
        {
            triggered = true;
            trig_comment = comment;
            trig_print = in + len / 2 - trig;   //Print after half of window and add offset
        }
    }

    /**
     * @brief Trigger and print data.
     */
    void trigger()
    {
        std::string n = "";
        trigger(n);
    }

    /**
     * @brief Put new sample in.
     * @param value store this value
     * @return true if window is gathered and is waiting to be printed
     */
    bool sample(const T& value)
    {
        if (wait_print == false)
        {
            //Store
            buffer[in % len] = value;
            in++;

            //Gathered enough
            if ((trig_print <= in) && triggered)
            {
                wait_print = true;
                return true;    //Full just now
            }
            return false;   //Not full yet
        }
        else
        {
            return true;    //Full already
        }
    }

    /**
     * @brief Print gathered data.
     */
    void pool_print()
    {
        if (wait_print)
        {
            //std::cout << trig_comment << std::endl;
            logfile << trig_comment << std::endl;
            logfile << name << " = [";   //Begin array
            for (unsigned int out = 0; out < len; out++)
            {
                print_one_sample(logfile, buffer[(in + out) % len]);    //Type specific, print number
                logfile << " ";   //Separate samples
            }
            logfile << "];" << std::endl; //End array

            triggered = false;
            wait_print = false;
        }
    }
};

/**
 * @brief Scope specialized for float.
 */
class ScopeFloat : public Scope<float>
{
    using Scope<float>::Scope;

    /**
     * @brief Function to print one sample to log.
     * @param logfile write to this stream
     * @param sample write this sample
     */
    virtual void print_one_sample(std::ofstream& logfile, float sample)
    {
        logfile << sample << " ";
    }
};

/**
 * @brief Scope specialized for complex float.
 */
class ScopeComplexFloat : public Scope<std::complex<float>>
{
    using Scope<std::complex<float>>::Scope;

    /**
     * @brief Function to print one sample to log.
     * @param logfile write to this stream
     * @param sample write this sample
     */
    virtual void print_one_sample(std::ofstream& logfile, const std::complex<float>& sample)
    {
        logfile << std::real(sample) << std::showpos << std::imag(sample) << std::noshowpos << "i ";
    }
};

/**
 * @brief Scope specialized for pair of int and float.
 */
class ScopeMarkedFloat : public Scope<std::pair<int,float>>
{
    using Scope<std::pair<int,float>>::Scope;

    /**
     * @brief Function to print one sample to log.
     * @param logfile write to this stream
     * @param sample write this sample
     */
    virtual void print_one_sample(std::ofstream& logfile, const std::pair<int,float>& sample)
    {
        logfile << sample.first << " " << sample.second << "; ";
    }
};

/**
 * @brief Scope specialized for pair of int and complex float.
 */
class ScopeMarkedComplexFloat : public Scope<std::pair<int,std::complex<float>>>
{
    using Scope<std::pair<int,std::complex<float>>>::Scope;

    /**
     * @brief Function to print one sample to log.
     * @param logfile write to this stream
     * @param sample write this sample
     */
    virtual void print_one_sample(std::ofstream& logfile, const std::pair<int,std::complex<float>>& sample)
    {
        logfile << sample.first << " " <<
            std::real(sample.second) << std::showpos << std::imag(sample.second) << std::noshowpos << "i; ";
    }
};

/**
 * @brief Scope specialized for pair of int and complex float.
 */
template<unsigned int N>
class ScopeNFloat : public Scope<std::array<float, N>>
{
    using Scope<std::array<float, N>>::Scope;

    /**
     * @brief Function to print one sample to log.
     * @param logfile write to this stream
     * @param sample write this sample
     */
    virtual void print_one_sample(std::ofstream& logfile, const std::array<float, N>& sample)
    {
        for (unsigned int i = 0; i < N; i++)
        {
            logfile << sample[i] << " ";
        }
        logfile << ";" << std::endl;
    }
};

#endif /* SCOPE_H_ */
